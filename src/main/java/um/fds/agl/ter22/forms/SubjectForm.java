package um.fds.agl.ter22.forms;

public class SubjectForm {

    private long id;

    private String title;

    private String Teacher;

    private String secondaryTeacher;

    public SubjectForm(long id, String title, String Teacher, String secondaryTeacher) {
        this.id = id;
        this.title = title;
        this.Teacher = Teacher;
        this.secondaryTeacher = secondaryTeacher;
    }

    public SubjectForm() {}

    public long getId() {return id;}
    public void setId(long id) { this.id = id; }
    public String getTitle() { return title; }
    public void setTitle(String title) { this.title = title; }
    public String getTeacher() { return Teacher; }
    public void setTeacher(String Teacher) { this.Teacher = Teacher; }
    public String getSecondaryTeacher() { return secondaryTeacher; }
    public void setSecondaryTeacher(String secondaryTeacher) { this.secondaryTeacher = secondaryTeacher; }

}
